FROM twalter/openshift-nginx:stable

# File Author / Maintainer
MAINTAINER Jose B. Gutierrez

# Delete the default configuration
#RUN rm -v /etc/nginx/nginx.conf

# Copy custom configuration file from the current directory
#COPY nginx.conf /etc/nginx/nginx.conf

COPY . /datas
